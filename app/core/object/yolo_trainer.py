#! /usr/bin/env python

import os,sys
import numpy as np
from app.util import parseAnnotation
from .yolo_detector import YOLODetector

# Need to test this setting
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="0"

class YOLOTrainer:
	"""
	Train YOLO model
	"""
	def __init__(self, settings, showLog=False, debugMode=False):
		self.settings = settings
		self.showLog = showLog
		self.debugMode = debugMode
		self.yolo = YOLODetector(architecture       = self.settings['model']['architecture'],
						input_size          = self.settings['model']['input_size'], 
						labels              = self.settings['model']['labels'], 
						max_box_per_image   = self.settings['model']['max_box_per_image'],
						anchors             = self.settings['model']['anchors'],
						early_stop          = self.settings['train']['early_stop'],
						showLog				= True)

		# Load pretraine weights (if exists)
		if os.path.exists(self.settings['train']['pretrained_weights']):
			print("[Info] Loading pre-trained weights in", self.settings['train']['pretrained_weights'])
			self.yolo.load_weights(self.settings['train']['pretrained_weights'])

	def train(self):
		# Parse annotation for testing set and training set
		train_imgs, train_labels = parseAnnotation(self.settings['train']['train_annot_folder'], 
													self.settings['train']['train_image_folder'], 
													self.settings['model']['labels'])

		# parse annotations of the validation set, if any, otherwise split the training set
		if os.path.exists(self.settings['valid']['valid_annot_folder']):
			valid_imgs, valid_labels = parseAnnotation(self.settings['valid']['valid_annot_folder'], 
														self.settings['valid']['valid_image_folder'], 
														self.settings['model']['labels'])
		else:
			train_valid_split = int(0.8*len(train_imgs))
			np.random.shuffle(train_imgs)

			valid_imgs = train_imgs[train_valid_split:]
			train_imgs = train_imgs[:train_valid_split]

		if len(self.settings['model']['labels']) > 0:
			overlap_labels = set(self.settings['model']['labels']).intersection(set(train_labels.keys()))

			if self.showLog:
				print('[Info] Seen labels:\t', train_labels)
				print('[Info] Given labels:\t', self.settings['model']['labels'])
				print('[Info] Overlap labels:\t', overlap_labels)

			if len(overlap_labels) < len(self.settings['model']['labels']):
				print('[Err] Some labels have no annotations! Please revise the list of labels in the settings.json file!')
				return
		else:
			print('[Err] No labels are provided. Train on all seen labels.')
			self.settings['model']['labels'] = train_labels.keys()
		
		self.yolo.train(train_imgs         = train_imgs,
				   valid_imgs         = valid_imgs,
				   train_times        = self.settings['train']['train_times'],
				   valid_times        = self.settings['valid']['valid_times'],
				   nb_epoch           = self.settings['train']['nb_epoch'], 
				   learning_rate      = self.settings['train']['learning_rate'], 
				   batch_size         = self.settings['train']['batch_size'],
				   warmup_epochs      = self.settings['train']['warmup_epochs'],
				   object_scale       = self.settings['train']['object_scale'],
				   no_object_scale    = self.settings['train']['no_object_scale'],
				   coord_scale        = self.settings['train']['coord_scale'],
				   class_scale        = self.settings['train']['class_scale'],
				   saved_weights_name = self.settings['train']['saved_weights_name'],
				   num_early_stop     = self.settings['train']['early_stop'],
				   debug              = self.settings['train']['debug'])