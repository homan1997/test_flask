import json
import os,sys
import math,cv2
import numpy as np
import tensorflow as tf
import keras
from keras.models import Model
from keras.callbacks import EarlyStopping, ModelCheckpoint, TensorBoard, CSVLogger
from keras.layers import Reshape, Conv2D, Input, Lambda
from keras.layers.merge import concatenate
from keras.optimizers import SGD, Adam, RMSprop
from app.core.algo import BatchGenerator, BoundingBox
from app.core.model import FullYOLO,TinyYOLO,MobileNet,SqueezeNet,InceptionV3,VGG16,ResNet50
from app.util import sigmoid,softmax,bbox_iou,checkCreate
import matplotlib.pyplot as plt
import datetime
from app.common import moduleController
from app.core.flowError import error
import requests

class LossHistory(keras.callbacks.Callback):

    def on_train_begin(self, logs={}):
        self.losses = {'batch':[], 'epoch':[]}
        self.accuracy = {'batch':[], 'epoch':[]}
        self.val_loss = {'batch':[], 'epoch':[]}
        self.val_acc = {'batch':[], 'epoch':[]}
        self.time = datetime.datetime.now()
        moduleController.result['remaining_epoch'] = moduleController.converter.setting['train']['nb_epoch']
 
    def on_batch_end(self, batch, logs={}):
        self.losses['batch'].append(logs.get('loss'))
        self.accuracy['batch'].append(logs.get('acc'))
        self.val_loss['batch'].append(logs.get('val_loss'))
        self.val_acc['batch'].append(logs.get('val_acc'))
        
    def on_epoch_begin(self, epoch, logs={}):
        self.epoch_time_start = datetime.datetime.now()
        
    def on_epoch_end(self, batch, logs={}):
        self.losses['epoch'].append(logs.get('loss'))
        self.accuracy['epoch'].append(logs.get('acc'))
        self.val_loss['epoch'].append(logs.get('val_loss'))
        self.val_acc['epoch'].append(logs.get('val_acc'))
        self.time = datetime.datetime.now() - self.epoch_time_start
        moduleController.result['each_epoch_time'] = float(self.time.total_seconds())
        moduleController.result['remaining_epoch']  -= 1 
        error()

    def loss_plot(self, loss_type):
        iters = range(len(self.losses[loss_type]))
        plt.figure()
        # acc
        plt.plot(iters, self.accuracy[loss_type], 'r', label='train acc')
        # loss
        plt.plot(iters, self.losses[loss_type], 'g', label='train loss')
        if loss_type == 'epoch':
            # val_acc
            plt.plot(iters, self.val_acc[loss_type], 'b', label='val acc')
            # val_loss
            plt.plot(iters, self.val_loss[loss_type], 'k', label='val loss')
        plt.grid(True)
        plt.xlabel(loss_type)
        plt.ylabel('acc-loss')
        plt.legend(loc="upper right")
        plt.show()