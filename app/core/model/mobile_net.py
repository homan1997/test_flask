#! /usr/bin/env python

from keras.models import Model
from keras.applications.mobilenet import MobileNet as MN
from keras.layers import Input
from .baseModel import BaseModel

__copyright__ = "Copyright 2019, QBS Limited"
__version__ = "1.0.0"

MODEL_PATH  = "app/static/model/mobilenet_wo_top.h5"

class MobileNet(BaseModel):
	"""
	MobileNet
	"""
	def __init__(self, input_size):
		input_image = Input(shape=(input_size, input_size, 3))

		mobilenet = MN(input_shape=(224,224,3), include_top=False)
		mobilenet.load_weights(MODEL_PATH)

		x = mobilenet(input_image)

		self.feature_extractor = Model(input_image, x)  

	def normalize(self, image):
		image = image / 255.
		image = image - 0.5
		image = image * 2.

		return image