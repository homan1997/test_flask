#! /usr/bin/env python

from keras.models import Model
from keras.applications.resnet50 import ResNet50 as RN50
from .baseModel import BaseModel

__copyright__ = "Copyright 2019, QBS Limited"
__version__ = "1.0.0"

MODEL_PATH  = "app/static/model/resnet50_wo_top.h5"

class ResNet50(BaseModel):
	"""
	ResNet50 Model
	"""
	def __init__(self, input_size):
		resnet50 = RN50(input_shape=(input_size, input_size, 3), include_top=False)
		resnet50.layers.pop() # remove the average pooling layer
		resnet50.load_weights(MODEL_PATH)

		self.feature_extractor = Model(resnet50.layers[0].input, resnet50.layers[-1].output)

	def normalize(self, image):
		image = image[..., ::-1]
		image = image.astype('float')

		image[..., 0] -= 103.939
		image[..., 1] -= 116.779
		image[..., 2] -= 123.68

		return image 