#! /usr/bin/env python
import json
import os
from app.core.gen_anchors import main, setArg

# This line must be executed before loading Keras model.

#filename = 'setting.json'
def change(setting):
    args = setArg().parse_args()
    str_anchor = main(setting)
    chword = '{'+str_anchor+'}'
    chStr =json.loads(chword)
    setting['model']['anchors'] = chStr.pop('anchors')
    return setting

