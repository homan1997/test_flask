import os,sys,json,time
import tensorflow as tf
from keras import backend as K
from keras.models import load_model
from app.core.object.yolo_detector import YOLODetector
from tensorflow.python.framework import graph_util
from tensorflow.python.framework import graph_io
import keras

__copyright__ = "Copyright 2019,QBS Limited"
__version__ = "1.0.0"

out_name = []
class Extension_converter:
             
        def __init__(self):
                self.setting = None
                
        def freeze_session(session, keep_var_names=None, output_names=None, clear_devices=True):
                from tensorflow.python.framework.graph_util import convert_variables_to_constants
                graph = session.graph
                with graph.as_default():
                        freeze_var_names = list(set(v.op.name for v in tf.global_variables()).difference(keep_var_names or []))
                        output_names = output_names or []
                        output_names += [v.op.name for v in tf.global_variables()]
                        global out_name
                        out_name = output_names
                        # Graph -> GraphDef ProtoBuf
                        input_graph_def = graph.as_graph_def()
                        if clear_devices:
                                 for node in input_graph_def.node:
                                        node.device = ""
                        frozen_graph = convert_variables_to_constants(session, input_graph_def, output_names, freeze_var_names)
                return frozen_graph

        def keras2pb(self, name):
                K.set_learning_phase(0)
                K.clear_session()
                model = load_model(name, compile=False, custom_objects={'tf': tf})
                output_model_file = name.split(".")[0] + '.pb'
                frozen_graph = Extension_converter.freeze_session(K.get_session(), output_names=[out.op.name for out in model.outputs])
                graph_io.write_graph(frozen_graph, "", output_model_file, as_text=False)

        def pb2tflite(self, name):
                K.set_learning_phase(0)
                converter = from_keras_model(None, name)
                tflite_model = converter.convert()
                open(name.split(".")[0] + '.tflite', "wb").write(tflite_model)
        
        def from_keras_model(cls, model_file, input_arrays=None,input_shapes=None,output_arrays=None):
                keras.backend.clear_session()
                keras.backend.set_learning_phase(False)
                keras_model = keras.models.load_model(model_file, custom_objects={'tf': tf})
                sess = keras.backend.get_session()
                # Get input and output tensors.
                input_tensors = keras_model.inputs
                output_tensors = keras_model.outputs
                graph_def = freeze_session(sess, output_tensors)
                return tf.lite.TFLiteConverter.from_session(sess, input_tensors, output_tensors)
