#! /usr/bin/env python

import os,sys,json,time
import cv2
from app.core.object.yolo_detector import YOLODetector
import tensorflow as tf
from keras import backend as K
from tensorflow.python.framework import graph_util
from tensorflow.python.framework import graph_io
from app.common import moduleController
import threading
import requests

box_count = 0

class test_mp4(threading.Thread):
    	def __init__(self, settings):
    		threading.Thread.__init__(self)
    		self.setting = settings
    		self.mp4name = "tmp/tmp.mp4"
		
		
    	def run(self):
    		K.set_learning_phase(0)
    		K.clear_session()
    		model_arch = self.setting['model']['architecture']
    		#model_labels = ["person"]
    		model_labels = self.setting['model']['labels']
    		#video_path = "static/video/cam01_20180412_084932.avi"
    		video_path = self.mp4name
    		#video_path = 0
    		#weights_path = "mn_person.h5"
    		#weights_path = "full_person.h5"
    		weights_path = self.setting['train']['saved_weights_name']
    		#weights_path = "tiny_person.h5"
    		#weights_path = "full_person_new.h5"
    		#weights_path = "res50_yolo.h5"
    		#weights_path = "incep_v3_yolo.h5"
    		#weights_path = "mn_yolo.h5"

    		cap = cv2.VideoCapture(video_path)
    		# init writer parameters
    		fourcc = cv2.VideoWriter_fourcc(*'XVID')
    		writer = cv2.VideoWriter("tmp/test.mp4",fourcc, cap.get(cv2.CAP_PROP_FPS), (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)),int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))))
    		#writer = cv2.VideoWriter("car1.avi",fourcc, 5, (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)),int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))))
    		#cap = cv2.VideoCapture(0)
    		#person_class_label = 13 # full yolo
    		person_class_label = 13 # other
    		box_count = 0
    		fps_sum = 0
    		frame_count = 0
    		ratio = 1.
    		yolo = YOLODetector(architecture    = model_arch,
						input_size          = self.setting['model']['input_size'], 
						labels              = model_labels, 
						max_box_per_image   = self.setting['model']['max_box_per_image'],
						anchors             = self.setting['model']['anchors'],
						showLog				= True,
						early_stop 			= self.setting['train']['early_stop'])
    		yolo.load_weights(weights_path)
    		print ("Weight loaded from sucessfully from {}".format(weights_path))
    		while cap.isOpened():
    				start_time = time.time()
    				ret, image = cap.read()
    				if image is None:
    						break
    				output = image.copy()
    				boxes = yolo.predict(image)
    				save = True
    				#for b in boxes:
    					# if b.w > 0.7:
    					# 	save = True
    					#print(b.label,"{},{}".format(setting['model']['labels'][b.label],b.score))
    					#print("({},{}),({},{})".format(b.xmin,b.ymin,b.xmax,b.ymax))
    				image = test_mp4.draw_boxes(image, boxes, model_labels, self.setting)
    				#print('{} boxes are found'.format(len(boxes)))
    				image = cv2.resize(image, (int(image.shape[1]/ratio),int(image.shape[0]/ratio)))
    				writer.write(image)
    				if save:
    						cv2.imwrite("error/{}.jpg".format(frame_count), output)
    				if cv2.waitKey(1) & 0xFF == ord('q'):
    						break
    				fps = 1./(time.time()-start_time)
    				#print("FPS:{}".format(fps))
    				fps_sum += fps
    				frame_count += 1
    				#print("FPS:{}".format(1./(time.time()-start_time)))

    		print("Found {} box from {} frame, avg fps = {}".format(box_count,frame_count,(fps_sum/frame_count)))
    		cap.release()
    		writer.release()
    		cv2.destroyAllWindows()
    		files = {'file': open('tmp/test.mp4', 'rb')}
    		values = { 'task_id' : moduleController.result['task_id']}
    		response = requests.post(moduleController.test_url, files=files, data = values)
    		print('status code : ', response.status_code)
		
    	def draw_boxes(image, boxes, labels, setting):
    		global box_count
    		"""
    		Tmp function to draw boxes
    		"""
    		for box in boxes:
    				# if box.conf <= 0.45:
    				# 	continue
    				# if box.label != person_class_label:
    				# 	continue
    				xmin  = int((box.cx - box.w/2) * image.shape[1])
    				xmax  = int((box.cx + box.w/2) * image.shape[1])
    				ymin  = int((box.cy - box.h/2) * image.shape[0])
    				ymax  = int((box.cy + box.h/2) * image.shape[0])
    				#print(xmin,xmax,ymin,ymax)
    				print(box.label,"{},{}".format(setting['model']['labels'][box.label],box.score))
    				cv2.rectangle(image, (xmin,ymin), (xmax,ymax), (0,255,0), 3)
    				# cv2.putText(image, 
    				# 			labels[box.getLabel()] + ' ' + str(box.getScore()), 
    				# 			(xmin, ymin - 13), 
    				# 			cv2.FONT_HERSHEY_SIMPLEX, 
    				# 			1e-3 * image.shape[0], 
    				# 			(0,255,0), 2)
    				cv2.putText(image, 
    				 			labels[box.getLabel()] + ' ' + str(box.getScore()), 
    				 			(xmin, int(ymin + (ymax-ymin)/2)), 
    				 			cv2.FONT_HERSHEY_SIMPLEX, 
    				 			1e-3 * image.shape[0], 
    				 			(0,255,0), 2)
    				box_count += 1
			
    		return image       
