import json

def validsetting(setting):
        if not setting['model']['architecture'] or not isinstance(setting['model']['architecture'], str):
                return True
        if not setting['model']['input_size'] or not isinstance(setting['model']['input_size'], int):
                return True
        if not setting['model']['max_box_per_image'] or not isinstance(setting['model']['max_box_per_image'], int):
                return True
        if not setting['model']['labels'] or not isinstance(setting['model']['labels'], list):
                return True
        if not setting['train']['train_image_folder'] or not isinstance(setting['train']['train_image_folder'], str):
                return True
        if not setting['train']['train_annot_folder'] or not isinstance(setting['train']['train_annot_folder'], str):
                return True
        if not setting['train']['train_times'] or not isinstance(setting['train']['train_times'], int):
                return True
        if not isinstance(setting['train']['pretrained_weights'], str):
                return True
        if not setting['train']['batch_size'] or not isinstance(setting['train']['batch_size'], int):
                return True
        if not setting['train']['learning_rate'] or not isinstance(setting['train']['learning_rate'], float):
                return True
        if not setting['train']['nb_epoch'] or not isinstance(setting['train']['nb_epoch'], int):
                return True
        if  not isinstance(setting['train']['warmup_epochs'], int):
                return True
        if not setting['train']['object_scale'] or not isinstance(setting['train']['object_scale'], float):
                return True
        if not setting['train']['no_object_scale'] or not isinstance(setting['train']['no_object_scale'], float):
                return True
        if not setting['train']['coord_scale'] or not isinstance(setting['train']['coord_scale'], float):
                return True
        if not setting['train']['class_scale'] or not isinstance(setting['train']['class_scale'], float):
                return True
        if not setting['train']['saved_weights_name'] or not isinstance(setting['train']['saved_weights_name'], str):
                return True
        if not setting['train']['debug'] or not isinstance(setting['train']['debug'], bool):
                return True
        if not setting['train']['early_stop'] or not isinstance(setting['train']['early_stop'], int):
                return True
        if not isinstance(setting['valid']['valid_image_folder'], str):
                return True
        if not isinstance(setting['valid']['valid_annot_folder'], str):
                return True
        if not setting['valid']['valid_times'] or not isinstance(setting['valid']['valid_times'], int):
                return True
        return False