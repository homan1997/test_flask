from __future__ import print_function
import datetime
import cv2
import numpy as numpy
import argparse, json
from app.util import basic_args
from app.core.object import YOLOTrainer
import keras
from app.core.ch_anchors import change
from app.core.convert import Extension_converter
import tensorflow as tf
from keras import backend as K
import threading
from app.common import moduleController
from app.core.flowError import error
import requests

__copyright__ = "Copyright 2019,QBS Limited"
__version__ = "1.0.0"

class trainModel(threading.Thread):
    	
	def __init__(self, settings):
    		threading.Thread.__init__(self)
    		self.setting = settings
	
	def run(self):	
		try:
			moduleController.result['isTerminated'] = False
			if not(self.setting['train']['pretrained_weights']):
				setting = change(self.setting)
			K.clear_session()
			setting = self.setting
			setting['model']['labels'] = [ x for x in setting['model']['labels'][0].split(",")]
			moduleController.result['training_status'] = 'Training'
			trainer = YOLOTrainer(setting, showLog=True)
			trainer.train()
			moduleController.result['training_status'] = 'Testing'
			error()
		except (ValueError, AssertionError) as err: 
			print("something wrong, training is stop!")
			print(err)
			print(err.args)
			moduleController.result['has_exception'] = True
			error()
