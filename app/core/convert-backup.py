import os,sys,json,time
import tensorflow as tf
from keras import backend as K
from keras.models import load_model
from app.core.object.yolo_detector import YOLODetector
from tensorflow.python.framework import graph_util
from tensorflow.python.framework import graph_io


__copyright__ = "Copyright 2019,QBS Limited"
__version__ = "1.0.0"

class Extension_converter:
   
        def __init__(self):
                self.setting = None
                self.out_name = []
                self.in_name = []

        def keras2pb(self, setting, name):
                K.set_learning_phase(0)
                K.clear_session()
                weights_path = name
                output_dir = ""
                output_model_file = weights_path.split(".")[0] + '.pb'
                # Convert Parameters
                output_node_prefix = "output_node"
                num_output = 1
                
                yolo = YOLODetector(architecture        = setting['model']['architecture'],
                                                
                                                input_size          = setting['model']['input_size'], 
                                                labels              = setting['model']['labels'], 
                                                max_box_per_image   = setting['model']['max_box_per_image'],
                                                anchors             = setting['model']['anchors'],
                                                showLog				= False)
                yolo.load_weights(weights_path)
                print ("Weight loaded from sucessfully from {}".format(weights_path))
                
                pred = [None]*num_output
                pred_node_names = [None]*num_output
                for i in range(num_output):
                        pred_node_names[i] = output_node_prefix+str(i)
                        pred[i] = tf.identity(yolo.model.outputs[i], name=pred_node_names[i])
                print('Found {} output nodes.'.format(len(pred_node_names)))
                self.out_name = pred_node_names
                print('Output node names: ', pred_node_names)
                
                # MUST BE DONE AFTER IDENTITY 
                sess = K.get_session()
                graph_def = sess.graph.as_graph_def()
                constant_graph = graph_util.convert_variables_to_constants(sess, graph_def, pred_node_names)
                graph_io.write_graph(constant_graph, output_dir, output_model_file, as_text=False) 

        def pb2tflite(self,setting, name): 
                ### need convert h5 to pb first ### 
                K.set_learning_phase(0)
                gf = tf.GraphDef()
                m_file = open(name.split(".")[0] + '.pb','rb')
                gf.ParseFromString(m_file.read())
                in_name = [gf.node[2].name]

                graph_def_file = (name.split(".")[0] + '.pb')
                """converters = tf.lite.TFLiteConverter.from_frozen_graph(
                        graph_def_file, ['input_1'], ['output_node0'])"""
                converters = tf.lite.TFLiteConverter.from_frozen_graph(
                        graph_def_file, in_name, self.out_name)
                tflite_model = converters.convert()
                open(name.split(".")[0] + '.tflite', "wb").write(tflite_model)

