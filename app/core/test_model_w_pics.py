
import os,sys,json,time
import cv2
from app.core.object.yolo_detector import YOLODetector
import tensorflow as tf
from keras import backend as K
from tensorflow.python.framework import graph_util
from tensorflow.python.framework import graph_io
import re
import base64
from io import BytesIO
from PIL import Image
import threading
import requests
from app.common import moduleController

box_count = None

class test_pics(threading.Thread):
	def __init__(self, settings):
    		threading.Thread.__init__(self)
    		self.setting = settings
    		self.imgstring = "tmp/tmp.jpg"
			
	def run(self):
		K.set_learning_phase(0)
		K.clear_session()
		#weights_path = "mn_person.h5"
		weights_path = self.setting['train']['saved_weights_name']
		#weights_path="sq_person.h5"
		yolo = YOLODetector(architecture        = self.setting['model']['architecture'],
						input_size          = self.setting['model']['input_size'], 
						labels              = self.setting['model']['labels'], 
						max_box_per_image   = self.setting['model']['max_box_per_image'],
						anchors             = self.setting['model']['anchors'],
						showLog				= True,
						early_stop 			= self.setting['train']['early_stop'])
		yolo.load_weights(weights_path)
		print ("Weight loaded from sucessfully from {}".format(weights_path))

		# ======================================== Testing ================================

		image = cv2.imread(self.imgstring)
		boxes = yolo.predict(image)
		image = test_pics.draw_boxes(image, boxes, self.setting['model']['labels'])
			#print('{} boxes are found'.format(len(boxes)))
		image = cv2.resize(image, (int(image.shape[1]),int(image.shape[0])))
		cv2.imwrite(self.imgstring,image)
		cv2.destroyAllWindows()
		files = {'file': open('tmp/tmp.jpg', 'rb')}
		values = { 'task_id' : moduleController.result['task_id']}
		response = requests.post(moduleController.test_url, files=files, data = values)
		print('status code : ', response.status_code)
		

	def draw_boxes(image, boxes, labels):
		global box_count
		"""
		Tmp function to draw boxes
		"""
		for box in boxes:
			#print(box.conf,box.cx,box.cy,box.w,box.h,box.xmin,box.ymin,box.xmax,box.ymax,box.classes)
			if box.conf <= 0.8:
				continue

			xmin  = int((box.cx - box.w/2) * image.shape[1])
			xmax  = int((box.cx + box.w/2) * image.shape[1])
			ymin  = int((box.cy - box.h/2) * image.shape[0])
			ymax  = int((box.cy + box.h/2) * image.shape[0])


			print(ymin,xmin,ymax,xmax)
			cv2.rectangle(image, (xmin,ymin), (xmax,ymax), (0,255,0), 3)
			cv2.putText(image, 
						labels[box.getLabel()] + ' ' + str(box.getScore()), 
						(xmin, ymin - 13), 
						cv2.FONT_HERSHEY_SIMPLEX, 
						1e-3 * image.shape[0], 
						(0,255,0), 2)
			
		return image       
