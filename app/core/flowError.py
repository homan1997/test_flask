import json
import requests
from app.common import moduleController

def error():
    result = json.dumps(moduleController.result)
    response = requests.post(moduleController.api_url, data = result , headers={'Content-type': 'application/json'})
    print('status code : ', response.status_code)