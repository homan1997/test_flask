#! /usr/bin/env python
import os, json, datetime
from flask import request, render_template, make_response, send_from_directory
from app import app
from app.core.train_model import trainModel
from app.core.convert import Extension_converter
from app.core.test_model_w_webcam import test_mp4
from app.core.test_model_w_pics import test_pics
from app.core.object.yolo_detector import getTime
from app.core.validation import validsetting
import datetime
import threading
import requests

__author__ = "Andy Tsang"
__credits__ = ["Andy Tsang"]
__version__ = "0.0.0"
__maintainer__ = "Andy Tsang"
__email__ = "atc1992andy@gmail.com"

DT_FORMAT = "%d-%m-%Y %H:%M:%S"

converter = Extension_converter()

api_url = "http://172.20.10.3:9020/admin/train/remainingTime"

test_url = "http://172.20.10.3:9020/admin/test/upload"

result = {
"each_epoch_time" : 60,
"remaining_epoch" : 0,
"has_exception" : False,
"training_status" : "Training",
"task_id" : ""
}


@app.after_request
def add_header(response):
	response.cache_control.max_age = 60
	if 'Cache-Control' not in response.headers:
		response.headers['Cache-Control'] = 'no-store'
	return response


@app.route('/training/', methods=['POST'])
def training():
    setting = request.get_json()
    try:
        if validsetting(setting):
            response = app.response_class(response=json.dumps({"result":"input error"}),status=200,mimetype='application/json')
            return response
    except:
        response = app.response_class(response=json.dumps({"result":"input error"}),status=200,mimetype='application/json')
        return response
    setting['train']['saved_weights_name'] = ("app/model/" + setting['train']['saved_weights_name'])
    converter.setting = setting
    result['task_id'] = converter.setting['task_id']
    tm = trainModel(setting)
    tm.start()
    response = app.response_class(
        response=json.dumps({"result":"okay"}),
        status=200,
        mimetype='application/json'
    )
    return response


@app.route('/download/model/', methods=['POST'])
def downloadModel():
    setting = converter.setting
    name = request.get_json()
    converter.keras2pb(setting['train']['saved_weights_name'])
    converter.pb2tflite(setting['train']['saved_weights_name'])
    return (setting['train']['saved_weights_name'] + "." + name)


@app.route('/testing/', methods=['POST'])
def testing():
    setting = converter.setting
    file_data = request.get_json()
    r = requests.get(file_data['file'])
    print(r.headers.get("Content-Type"))
    
    if r.headers.get("Content-Type")=="image/jpeg":
        with open('tmp/tmp.jpg', 'wb') as f:
            f.write(r.content)
        tp = test_pics(setting)
        tp.start()

    if r.headers.get("Content-Type")=="video/mp4":
        with open('tmp/tmp.mp4', 'wb') as f:
            f.write(r.content)
        t4 = test_mp4(setting)
        t4.start()  
    img_result = app.response_class(
        response=json.dumps({ "result": 'okay'}),
        status=200,
        mimetype='application/json'
    )
    return img_result

@app.route('/demo/testing/', methods=['GET'])
def demoTest():
    with open('app/demo/setting.json', 'r') as f:
        setting = json.load(f)
    setting['train']['saved_weights_name'] = ("app/demo/" + setting['train']['saved_weights_name'])
    file_data = request.get_json()
    r = requests.get(file_data['file'])
    print(r.headers.get("Content-Type"))
    
    if r.headers.get("Content-Type")=="image/jpeg":
        with open('tmp/tmp.jpg', 'wb') as f:
            f.write(r.content)
        tp = test_pics(setting)
        tp.start()

    if r.headers.get("Content-Type")=="video/mp4":
        with open('tmp/tmp.mp4', 'wb') as f:
            f.write(r.content)
        t4 = test_mp4(setting)
        t4.start()  
    img_result = app.response_class(
        response=json.dumps({ "result": 'okay'}),
        status=200,
        mimetype='application/json'
    )
    return img_result

@app.route("/download/file/", methods=['GET'])
def download_file():
    directory = os.getcwd()
    return send_from_directory(directory, "tmp/tmp.mp4", mimetype="video/mp4", as_attachment=True)