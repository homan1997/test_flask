#! /usr/bin/env python

import io,os,sys
import numpy as np

__copyright__ = "Copyright 2019, QBS Limited"
__version__ = "1.0.2"

def npa2str(nparray):
	"""
	Convert a np.array to bytes string
	Args:
		nparray (np.array):
	Returns:
		data (string):
	"""
	output = io.BytesIO()
	np.savez(output, x=nparray)
	data = output.getvalue()

	return data

def str2npa(s):
	"""
	Convert a bytes string to np.array
	Args:
		s (string):
	Returns:
		nparray (np.array):
	"""
	return np.load(io.BytesIO(s))['x']

def sigmoid(x):
	"""
	Sigmoid operation
	"""
	if type(x) is not float and type(x) is not int:
		x = x.astype(dtype=np.float)
	y = 1. / (1. + np.exp(-x))
	
	return y

def softmax(x, axis=-1, t=-100.):
	"""
	Softmax operation
	"""
	x = x - np.max(x)
	if np.min(x) < t:
		x = x/np.min(x)*t
		
	e_x = np.exp(x)

	return e_x / e_x.sum(axis, keepdims=True)

if __name__ == "__main__":
	x = np.arange(1280).reshape(128,10)
	print(x.dtype)
	print(x.tostring())
	#s = npa2str(x)
	#print(str2npa(s))

