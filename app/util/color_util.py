#! /usr/bin/env python
import numpy as np
from random import shuffle

__copyright__ = "Copyright 2019, QBS Limited"
__version__ = "1.0.0"

GOLDEN_RATIO = 0.618033988749895

def genColors(num, isShuffle=False):
	"""
	Generate a list of color in RGB
	Args:
	Return:
	"""
	colors = []
	for i in range(num):
		h = 1. / num * i
		h += GOLDEN_RATIO
		h %= 1
		r, g, b = hsv2rgb(h, 0.9, 0.9)
		colors.append(rgb2hex(r, g, b))

	if isShuffle:
		shuffle(colors)

	return colors

def hsv2rgb(h, s, v):
	"""
	Convert color space from HSV to RGB
	Args:
		h (float):
		s (float):
		v (float):
	Return:
		r (int):
		g (int):
		b (int):
	"""
	h_i = int(h*6)
	f = h*6 - h_i
	p = v * (1 - s)
	q = v * (1 - f*s)
	t = v * (1 - (1 - f) * s)
	if h_i == 0:
		r, g, b = v, t, p
	if h_i == 1:
		r, g, b = q, v, p
	if h_i == 2:
		r, g, b = p, v, t
	if h_i == 3:
		r, g, b = p, q, v
	if h_i == 4:
		r, g, b = t, p, v
	if h_i == 5:
		r, g, b = v, p, q

	return int(r*256), int(g*256), int(b*256)

def rgb2hex(r, g, b):
	"""
	Convert RGB to color hex
	Args:
		r (int):
		g (int):
		b (int):
	Returns:
		hex (string):
	"""
	return '#%02x%02x%02x' % (r, g, b)