#! /usr/bin/env python

import os
import numpy as np

__copyright__ = "Copyright 2019, QBS Limited"
__version__ = "1.0.0"

def normalize(image):
    """
    Basic normalization for image
    Args:
        image (np.array): original image range 0~255
    Returns:
        image (np.array): normalized image range 0.~1.
    """
    image = image / 255.
    
    return image

def bbox_iou(box1, box2):
    """
    Calculating the IoU of two ht_vision.boundingBox
    Args:
        box1 (ht_vision.BoundingBox): Box 1
        box2 (ht_vision.BoundingBox): Box 2
    Returns:
        iou (float): Intersect over Union
    """
    x1_min  = box1.cx - box1.w/2
    x1_max  = box1.cx + box1.w/2
    y1_min  = box1.cy - box1.h/2
    y1_max  = box1.cy + box1.h/2
    
    x2_min  = box2.cx - box2.w/2
    x2_max  = box2.cx + box2.w/2
    y2_min  = box2.cy - box2.h/2
    y2_max  = box2.cy + box2.h/2
    
    intersect_w = interval_overlap([x1_min, x1_max], [x2_min, x2_max])
    intersect_h = interval_overlap([y1_min, y1_max], [y2_min, y2_max])
    
    intersect = intersect_w * intersect_h
    
    union = box1.w * box1.h + box2.w * box2.h - intersect
    
    if union <= 0:
        return 0
        
    return float(intersect) / union
    
def interval_overlap(interval_a, interval_b):
    """
    Return the minium coordinate from 2 given interval
    Args:
        interval_a (list): Interval a
        interval_b (list): Interval b
    """
    x1, x2 = interval_a
    x3, x4 = interval_b

    if x3 < x1:
        if x4 < x1:
            return 0
        else:
            return min(x2,x4) - x1
    else:
        if x2 < x3:
            return 0
        else:
            return min(x2,x4) - x3  