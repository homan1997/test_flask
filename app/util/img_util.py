#! /usr/bin/env python

import os,sys
import numpy as np
import base64
import cv2
from scipy import misc
from PIL import Image
from .ht_util import getCenterPoint

__copyright__ = "Copyright 2019, QBS Limited"
__version__ = "1.0.0"

def getDownSampleImage(image, imageSize=320):
	"""
	Function take image and down sample to maxium size
	Args:
		image (numpy.array): image use for cropping (h,w,c)
		imageSize (int): target size
	Returns:
		resized_image (numpy.array): cropped image (h,w,c)
	"""

	image_ratio = float(image.shape[1])/image.shape[0]
	if image_ratio > 1:
		resized_image = cv2.resize(image, (imageSize, int(imageSize/image_ratio)))
	else:
		resized_image = cv2.resize(image, (int(imageSize*image_ratio), imageSize))
	#print "Resized:{}".format(resized_image.shape)

	return resized_image


def getCroppedImage(image, locations, imageSize=128, isWithPadding=False):
	"""
	Function take image and bounding boxes return cropped images
	Args:
		image (numpy.array): image use for cropping (h,w,c)
		locations (numpy.array): face location information
		imageSize (int): size of the cropped image
		isWithPadding (boolean): the cropped image whether with padding
	Returns:
		images (numpy.array): array of cropped images (n,h,w,c)
	"""
	new_images = []
	if image is None or locations is None or len(locations) <= 0:
		return
	for loc in locations:
		y = t = loc[0]
		r = loc[1]
		b = loc[2]
		x = l = loc[3]
		width = r-l
		height = b-t
		face_center = getCenterPoint(loc)
		face_center = (int(face_center[0]),int(face_center[1]))
		if isWithPadding:
			# length(top,left,bot,right)
			lengths = [face_center[1],face_center[0],image.shape[0]-face_center[1],image.shape[1]-face_center[0]]
			min_length = min(lengths)
			crop_img = misc.imresize(image[(face_center[1]-min_length):(face_center[1]+min_length), (face_center[0]-min_length):(face_center[0]+min_length)], (imageSize,imageSize))
		else:
			crop_img = misc.imresize(image[y:(y+height), x:(x+width)], (imageSize,imageSize))
		new_images.append(crop_img)
	new_images = np.array(new_images)

	return new_images

def npa2base64(npa):
	"""
	Convert image (numpy.array) to base64
	Args:
		npa (numpy.array): image in numpy.arry
	Returns:
		b64 (string): image in base64
	"""
	return (base64.b64encode(np.ascontiguousarray(npa))).decode("utf-8")

def base642npa(b64, shape):
	"""
	Convert image (string.base64) to numpy array
	Args:
		b64 (string): image in base64
		shape (tuple): shape of the image (h,w,c)
	Returns:
		npa (numpy.array): image in numpy array
	"""
	npa = np.reshape(np.frombuffer(base64.decodestring(b64), dtype=np.uint8),shape)

	return npa

def saveImgRGB(savePath, image):
	"""
	Save RGB image by cv2
	Args:
		image (numpy.array): image in RGB
	"""
	cv2.imwrite(savePath,cv2.cvtColor(image,cv2.COLOR_RGB2BGR))	

def PIL2CV(image):
	"""
	Convert PIL.image to OpenCV image
	"""	
	image = np.array(image)
	image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

	return image

def CV2PIL(image):
	"""
	Convert OpenCV image to PIL.image
	"""
	image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
	image = Image.fromarray(image)

	return image						

if __name__ == "__main__":
	input = cv2.imread("../test.jpg")
	boxes = np.array([[0,0,30,30],[5,5,5,5]])
	images = getCroppedImage(input, boxes)

	print(images.shape)
